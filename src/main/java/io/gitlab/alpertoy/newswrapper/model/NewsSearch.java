package io.gitlab.alpertoy.newswrapper.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * User: alpertoy
 * Date: 21.02.2021
 * Time: 15:21
 */
public class NewsSearch implements Serializable {

    private static final long serialVersionUID = 1L;

    private Articles[] articles;

    public Articles[] getArticles() {
        return articles;
    }

    public void setArticles(Articles[] articles) {
        this.articles = articles;
    }

    @Override
    public String toString() {
        return "NewsSearch{" +
                "articles=" + Arrays.toString(articles) +
                '}';
    }
}

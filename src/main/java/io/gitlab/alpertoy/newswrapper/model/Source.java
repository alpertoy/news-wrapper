package io.gitlab.alpertoy.newswrapper.model;

import java.io.Serializable;

/**
 * User: alpertoy
 * Date: 21.02.2021
 * Time: 13:19
 */
public class Source implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Source{" +
                "name='" + name + '\'' +
                '}';
    }
}

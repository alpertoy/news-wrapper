package io.gitlab.alpertoy.newswrapper.model;

import java.io.Serializable;

/**
 * User: alpertoy
 * Date: 21.02.2021
 * Time: 13:19
 */

public class Articles implements Serializable {

    private static final long serialVersionUID = 1L;

    private Source source;
    private String title;
    private String description;
    private String urlToImage;

    public Source getSource() {
        return source;
    }

    public void setSource(Source source) {
        this.source = source;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrlToImage() {
        return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
        this.urlToImage = urlToImage;
    }

    @Override
    public String toString() {
        return "NewsSearch{" +
                "source=" + source +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", urlToImage='" + urlToImage + '\'' +
                '}';
    }
}

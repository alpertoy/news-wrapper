package io.gitlab.alpertoy.newswrapper.service;

import io.gitlab.alpertoy.newswrapper.model.NewsSearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * User: alpertoy
 * Date: 21.02.2021
 * Time: 13:15
 */

@Service
public class NewsService {

    private static final String API_KEY = "028d93c532a046e6a540f40e69294381";

    @Autowired
    private RestTemplate restTemplate;

    public NewsSearch indexNews(String country, String category) {
        String url = "https://newsapi.org/v2/top-headlines?country={country}&category={category}&apiKey="+API_KEY;
        return this.restTemplate.getForObject(url, NewsSearch.class, country, category);
    }


}

package io.gitlab.alpertoy.newswrapper.controller;

import io.gitlab.alpertoy.newswrapper.model.Articles;
import io.gitlab.alpertoy.newswrapper.model.NewsSearch;
import io.gitlab.alpertoy.newswrapper.service.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * User: alpertoy
 * Date: 21.02.2021
 * Time: 13:34
 */


@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @GetMapping
    public NewsSearch indexNews(@RequestParam(value = "country") String country, @RequestParam(value = "category") String category) {
        return newsService.indexNews(country, category);
    }

}
